const { app, BrowserWindow } = require('electron');

const electron = require('electron');


// Enable live reload for all the files inside your project directory
require('electron-reload')(__dirname);

function createWindow() {
    // Erstelle das Browser-Fenster.
    const win = new BrowserWindow({
        width: 1280,
        height: 1055,
        webPreferences: {
            nodeIntegration: true
        },

    })

    // und lade den Inhalt von Dashboard
    win.loadFile('./src/pages/Login.html')

    // Öffnen der DevTools.

    //win.webContents.openDevTools()
}


// Diese Methode wird aufgerufen, wenn Electron das Starten abgeschlossen hat und bereit ist, 
// ein Browser Fenster zu erstellen.
// Einige APIs können nur nach dem Auftreten dieses Events genutzt werden.
app.whenReady().then(createWindow)

// Schließt die App, sobald alle Fenster geschlossen werden, außer auf macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

// In dieser Datei kann der Rest des App-Codes für den main proccess eingefügt werden. Sie können den Code auch 
// auf mehrere Dateien aufteilen und diese hier einbinden.